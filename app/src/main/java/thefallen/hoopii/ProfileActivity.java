package thefallen.hoopii;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Outline;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    //Declaring Views and constants

    private static final int REQUEST_CODE = 1;
    ImageView profilepic,editBut;
    TextView uname,location,utype;
    Bitmap bm;
    private SharedPreferences sharedPreferences;
    private Context mContext;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        sharedPreferences = this.getSharedPreferences("userData", MODE_PRIVATE);
        mContext = this;

        //DP display
        profilepic = (ImageView) findViewById(R.id.dp);
        displayDp();

        //The Edit Button
        editBut = (ImageView) findViewById(R.id.button1);
        editBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });

        //UserName Text
        uname = (TextView) findViewById(R.id.uname);
        uname.setText(sharedPreferences.getString("firstName", "") + " " + sharedPreferences.getString("lastName", ""));

        //Setting up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Profile");

    }

    /*
        input : strID - int, retry - boolean
        output : void
        function : sets up a snackbar to print a message
     */
    void errorSnack(int strID, boolean retry) {
        Snackbar snackbar = Snackbar.make(editBut, strID, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    /*
        input : utype - String
        output : usertype - String
        function : Converts user type integer to one of the types
     */
    private String getUtype(String utype)
    {
        int u = Integer.parseInt(utype);
        switch (u)
        {
            case 0 :    return "SuperUser";
            case 1 :    return "Student";
            case 2 :    return "Employee";
            case 3 :    return "Worker";
            case 4 :    return "Warden";
            case 5 :    return "Dean";
            default:    return "";
        }
    }

    /*
        input : void
        output : void
        function : starts the activity to select an image
     */
    public void pickImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_CODE);
    }


    /*
        input : requestCode - int, resultCode - int, data - Intent
        output : void
        function : extracts the image from the result of the image picking
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    try {
                        bm = decodesmall(data.getData(),512);
                        callApi();
                         } catch (Exception e) {
                        Log.e("im", e.getLocalizedMessage());
                    }
                    break;
                }
        }
    }

    /*
        input : void
        output : void
        function : displays the dp initially
     */
    public void displayDp()
    {
        String url = APIdetails.dp()+"/"+sharedPreferences.getString("dpUrl","");
        ImageRequest request = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        profilepic.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        profilepic.setImageResource(R.drawable.avatar_42f2a3cb5734_128);
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authorization", sharedPreferences.getString("token", ""));
                return params;
            }
        };
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }

    /*
        input : uri - Uri, factor - int
        output : Bitmap
        function : standardises the image that we get
     */
    public Bitmap decodesmall(Uri uri,int factor) {
        ParcelFileDescriptor parcelFD = null;
        try {
            parcelFD = getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor imageSource = parcelFD.getFileDescriptor();

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(imageSource, null, o);

            // the new size we want to scale to

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp < factor && height_tmp < factor) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFileDescriptor(imageSource, null, o2);

        } catch (FileNotFoundException e) {
            // handle errors
        } finally {
            if (parcelFD != null)
                try {
                    parcelFD.close();
                } catch (IOException e) {
                    // ignored
                }
        }
        return null;
    }

    /*
        input : void
        output : void
        function : calls the api to change the dp
     */
    public void callApi()
    {
        if(bm==null) return;
        ArrayList<Bitmap> files = new ArrayList<>();
        files.add(bm);
        MultipartRequest multipartRequest = new MultipartRequest(APIdetails.updatedp(), null,"avatar",files, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
                    JSONObject jsonObject = (new JSONObject(new String(response.data)));
                    if (jsonObject.getBoolean("success")) {
                        errorSnack(R.string.dp_change_success, false);
                        sharedPreferencesEditor.putString("dpUrl", jsonObject.getString("dpUrl"));
                        sharedPreferencesEditor.apply();
                        profilepic.setImageBitmap(bm);
                    }
                    else
                        errorSnack(R.string.action_history,false);
                }
                catch (JSONException e) {
                    e.printStackTrace();
                    }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                errorSnack(R.string.dp_change_failed, false);
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authorization", sharedPreferences.getString("token",""));
                return params;
            }
        };

        VolleySingleton.getInstance(mContext).addToRequestQueue(multipartRequest);
    }

    // Handles the back button functionality
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }

}
