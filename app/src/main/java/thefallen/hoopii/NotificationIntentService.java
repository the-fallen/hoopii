package thefallen.hoopii;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NotificationIntentService extends IntentService {

    /**
     * A constructor is required, and must call the super IntentService(String)
     * constructor with a name for the worker thread.
     */
    Context mContext;
    public NotificationIntentService() {
        super("HelloIntentService");
    }

    /**
     * The IntentService calls this method from the default worker thread with
     * the intent that started the service. When this method returns, IntentService
     * stops the service, as appropriate.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        // Normally we would do some work here, like download a file.
        // For our sample, we just sleep for 5 seconds.
        mContext = this;
        try {
            callApi();
        } catch (Exception e) {
            // Restore interrupt status.
            Thread.currentThread().interrupt();
        }
    }
    SharedPreferences sharedPreferences;
    public void callApi()
    {
        Log.d("noti",System.currentTimeMillis()/1000+"");
        sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        StringRequest request = new StringRequest(Request.Method.GET,APIdetails.getHost()+"/notifications",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String respone) {
                        Log.d("noti",respone);
                        try {
                            JSONObject jsonObject = new JSONObject(respone);
                            JSONArray notifications = jsonObject.getJSONArray("notifications");

                            for (int i=0;i<notifications.length();i++) {
                                JSONObject notification = notifications.getJSONObject(i);
                                String title;
                                if(notification.getInt("type")==1) title = notification.getString("firstName")+" posted a new complaint -"+notification.getString("description");
                                else title = "@"+notification.getString("firstName")+": "+notification.getString("description");
                                Intent resultIntent = new Intent(mContext, MainActivity.class);
                                PendingIntent resultPendingIntent =
                                        PendingIntent.getActivity(
                                                mContext,
                                                0,
                                                resultIntent,
                                                PendingIntent.FLAG_UPDATE_CURRENT
                                        );
                                Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                                NotificationCompat.Builder mBuilder =
                                        new NotificationCompat.Builder(mContext)
                                                .setSmallIcon(R.drawable.ic_stat_notiicon)
                                                .setContentTitle("Hoopii")
                                                .setAutoCancel(true)
                                                .setSound(alarmSound)
                                                .setContentIntent(resultPendingIntent)
                                                .setContentText(title);
                                NotificationManager mNotifyMgr =
                                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
// Builds the notification and issues it.

                                int mNotificationId = 01;

                                mNotifyMgr.notify(mNotificationId, mBuilder.build());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        Intent alarmIntent = new Intent(NotificationIntentService.this, NotificationIntentService.class);
                        PendingIntent pendingIntent = PendingIntent.getService(NotificationIntentService.this, 1, alarmIntent, 0);
                        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 10000, pendingIntent);

                    }
                },
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        Intent alarmIntent = new Intent(NotificationIntentService.this, NotificationIntentService.class);
                        PendingIntent pendingIntent = PendingIntent.getService(NotificationIntentService.this, 1, alarmIntent, 0);
                        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 10000, pendingIntent);
                    }
                })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authorization", sharedPreferences.getString("token", ""));
                return params;
            }
        };
        VolleySingleton.getInstance(mContext).addToRequestQueue(request);
    }
}