package thefallen.hoopii;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.io.ByteArrayOutputStream;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class PostComplaint extends AppCompatActivity {


    SharedPreferences sharedPreferences;
    boolean[] toggleTags;
    AlertDialog.Builder alertDialogBuilder;
    Context mContext;
    ArrayList<imageItem> Bitmaps;
    ArrayList<Bitmap> Bitmaps2;
    GridImageAdapter adapter;
    private static final int REQUEST_CODE = 1;
    RecyclerView images;
    EditText _title;
    EditText _description;
    Spinner visibility;
    String[] tags =new String[]{"Water Issues","Earth","Fire Nation","Metal Benders","Wood","Wind","Thunder","Spirit"};
    Typeface tf;
    ProgressDialog progressDialog;
    boolean inProgress=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_complaint);
        mContext=this;

        toggleTags = new boolean[tags.length];
        sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        setupTags(tags);

        _title = (EditText)findViewById(R.id.title);
        _description = (EditText)findViewById(R.id.description);
        visibility = (Spinner) findViewById(R.id.visibility);

        Bitmaps = new ArrayList<>();
        Bitmaps2 = new ArrayList<>();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        alertDialogBuilder = new AlertDialog.Builder(mContext, R.style.AppthemeDialog);
        progressDialog = new ProgressDialog(PostComplaint.this,
                R.style.AppthemeDialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Uploading complaint...");

        images = (RecyclerView) findViewById(R.id.images);
        GridLayoutManager gridLayoutManager =
                new GridLayoutManager(mContext,2) {
                    @Override
                    public boolean canScrollVertically() {
                        return false;
                    }
                };
        images.setLayoutManager(gridLayoutManager);
        adapter = new GridImageAdapter(Bitmaps);
        images.setAdapter(adapter);

        images.addOnItemTouchListener(new RecyclerItemClickListener(mContext, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view,final int position) {
                alertDialogBuilder.setTitle("Delete Submission?")
                        .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                adapter.removeAt(position);
                                Bitmaps2.remove(position);
                                images.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DisplayHelper.dpToPx(200 * ((1 + Bitmaps.size()) / 2), mContext)));
                            }
                        })
                        .setCancelable(true)
                        .setNegativeButton("NO", null)
                        .show();
                }
        }));

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.post_complaint, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home)
            finish();
        else if (id == R.id.post)
        {if(!inProgress)postComplain();}
        else if (id == R.id.attach)
            pickImage();

        return super.onOptionsItemSelected(item);
    }
    @SuppressWarnings("NullPointerException")
    void setupTags(String[] tags)
    {
        LinearLayout tags_layout = (LinearLayout) findViewById(R.id.tags);
        int screenSize = DisplayHelper.getWidth(mContext);
        LinearLayout tags_row = new LinearLayout(mContext);
        tags_row.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        tags_row.setOrientation(LinearLayout.HORIZONTAL);
        ToggleButton tag = null;
        LinearLayout.LayoutParams tagParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int margin = DisplayHelper.dpToPx(10,mContext);
        int padding = DisplayHelper.dpToPx(15, mContext);
        tagParams.bottomMargin = margin;
        tagParams.leftMargin = margin;
        for(int i=0,j=0;i<tags.length+1;i++)
        {
            if(i==tags.length&&j<screenSize)
            {if(tag!=null&&tag.getParent()==null)tags_row.addView(tag);}
            else if(j<screenSize){
                if(tag!=null&&tag.getParent()==null) tags_row.addView(tag);
                tag = new ToggleButton (mContext);
                tag.setLayoutParams(tagParams);
                tag.setPadding(padding, margin / 2, padding, margin / 2);
                tag.setTextOn(tags[i]);
                tag.setText(tags[i]);
                tag.setTextOff(tags[i]);
                tag.setTextSize(2 * margin / 3);
                tag.setClickable(true);
                tag.setTag(i);
                tag.setTypeface(tf);
                tag.setTransformationMethod(null);
                tag.setBackground(getResources().getDrawable(R.drawable.roundedbg2));
                tag.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            buttonView.setTextColor(getResources().getColor(R.color.accent));
                            buttonView.setBackground(getResources().getDrawable(R.drawable.roundedbg));
                            toggleTags[(int) buttonView.getTag()] = true;
                            // The toggle is enabled
                        } else {
                            buttonView.setTextColor(getResources().getColor(R.color.black));
                            buttonView.setBackground(getResources().getDrawable(R.drawable.roundedbg2));
                            toggleTags[(int) buttonView.getTag()] = true;
                            // The toggle is disabled
                        }
                    }
                });
                tag.measure(0, 0);       //must call measure!
                j+=tag.getMeasuredWidth()+margin;
            }
            else {
                j=0;
                i--;
                tags_layout.addView(tags_row);
                tags_row = new LinearLayout(mContext);
                tags_row.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                tags_row.setOrientation(LinearLayout.HORIZONTAL);
                tags_row.addView(tag);
                tag.measure(0, 0);       //must call measure!
                j+=tag.getMeasuredWidth()+margin;
            }
        }
        tags_layout.addView(tags_row);

    }

    public void pickImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    try {
                        Bitmap bm = decodesmall(data.getData(),512);
                        Bitmap bm_original = decodesmall(data.getData(),1024);
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        bm.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                        Bitmaps2.add(bm_original);
                        String size;
                        float bits = bm_original.getByteCount()/4;
                        if(bits / 1024 >1024) size = String.format("%.1f", (bits / 1024)/1024) + " mb";
                        else size = String.format("%.1f",bits / 1024) + " kb";
                        Bitmaps.add(new imageItem(bm, size));
                        adapter.notifyDataSetChanged();
                        images.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, DisplayHelper.dpToPx(200 * ((1 + Bitmaps.size()) / 2), mContext)));
                    }catch (Exception e)
                    {
                        Log.e("pickImage",e.getLocalizedMessage());
                    }

                    break;

                }
        }
    }
    public Bitmap decodesmall(Uri uri,int factor) {
        ParcelFileDescriptor parcelFD = null;
        try {
            parcelFD = getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor imageSource = parcelFD.getFileDescriptor();

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(imageSource, null, o);

            // the new size we want to scale to

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp < factor && height_tmp < factor) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFileDescriptor(imageSource, null, o2);

        } catch (FileNotFoundException e) {
            // handle errors
        } finally {
            if (parcelFD != null)
                try {
                    parcelFD.close();
                } catch (IOException e) {
                    // ignored
                }
        }
        return null;
    }
    public void postComplain() {

        if (!validate()) {
            onSignupFailed();
            return;
        }

        progressDialog.show();

        callApi();
    }
    boolean validate()
    {
        boolean valid = true;

        String title = _title.getText().toString();
        String description = _description.getText().toString();

        if (title.isEmpty() || title.length() < 3) {
            _title.setError("");
            valid = false;
        } else {
            _title.setError(null);
        }

        if (description.isEmpty()) {
            _description.setError("");
            valid = false;
        } else {
            _description.setError(null);
        }
        return valid;
    }
    public void onSignupSuccess() {
        if(progressDialog.isShowing())progressDialog.dismiss();
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        if(progressDialog!=null)if(progressDialog.isShowing()) progressDialog.dismiss();
    }
    public void callApi()
    {
        inProgress=true;
        MultipartRequest multipartRequest = new MultipartRequest(APIdetails.addComplaint(), null,"images",Bitmaps2, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                onSignupSuccess();
                inProgress=false;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onSignupFailed();
                inProgress=false;
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("title",_title.getText().toString());
                params.put("description",_description.getText().toString());
                params.put("level",visibility.getSelectedItemPosition()+"");
                for (int i=0;i<toggleTags.length;i++) if(toggleTags[i]) params.put("array"+i+"_"+"tags",tags[i]);
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authorization", sharedPreferences.getString("token",""));
                return params;
            }
        };

        VolleySingleton.getInstance(mContext).addToRequestQueue(multipartRequest);
    }
}
