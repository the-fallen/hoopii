package thefallen.hoopii;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Interprets the data we get for the comments from the api and parse it into meaningful usable variables
 */

public class CommentParse {

    String name,description,dpUrl;
    Timestamp timestamp;

    public CommentParse(JSONObject jsonObject)
    {
        try {
            description = jsonObject.getString("body");
            name = jsonObject.getString("firstName")+" "+ jsonObject.getString("lastName");
            dpUrl = jsonObject.getString("dpUrl");
            timestamp = new Timestamp(Long.valueOf(jsonObject.getInt("timestamp"))*1000);
        }catch (Exception e){
            Log.e("mew",jsonObject.toString());
            e.printStackTrace();
        }
    }
    public CommentParse(String name,String description,String dpUrl)
    {
        this.name=name;
        this.description=description;
        this.dpUrl=dpUrl;
        timestamp = new Timestamp(System.currentTimeMillis());
    }
    public static ArrayList<CommentParse> CommentArray(JSONArray jsonArray)
    {
        ArrayList<CommentParse> commentParses = new ArrayList<>();
        for (int i=0;i<jsonArray.length();i++)
        {
            try {
                commentParses.add(new CommentParse(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Collections.reverse(commentParses);
        return commentParses;
    }

}
