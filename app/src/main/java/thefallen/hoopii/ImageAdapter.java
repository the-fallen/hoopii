package thefallen.hoopii;

/**
 * Created by Anil on 3/29/2016.
 */
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.io.FileOutputStream;
import java.util.ArrayList;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<Bitmap> bitmaps;

    // Constructor
    public ImageAdapter(Context c,ArrayList<Bitmap> bits){
        bitmaps=bits;
        mContext = c;
    }

    @Override
    public int getCount() {
        return bitmaps.size();
    }

    @Override
    public Object getItem(int position) {
        return bitmaps.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.compimg, parent, false);
        ImageView imageView = (ImageView)v.findViewById(R.id.img);
        imageView.setImageBitmap(bitmaps.get(position));
        return v;
    }

}