package thefallen.hoopii;

/**
 * Created by Anil on 3/16/2016.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.felipecsl.asymmetricgridview.library.Utils;
import com.felipecsl.asymmetricgridview.library.model.AsymmetricItem;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RVAdapterComplaints extends RecyclerView.Adapter<RVAdapterComplaints.ElementHolder> {

    public static class ElementHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView title;
        TextView name;
        TextView time;
        TextView upvotes;
        TextView downvotes;
        TextView description;
        ImageView dp;
        View postcomment;
        View upvote;
        View downvote;
        View resolve;
        LinearLayout comments;
        EditText addcomment;
        MyGridView gridView;
        LinearLayout tags;
        ElementHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cv);
            title = (TextView)itemView.findViewById(R.id.title);
            time = (TextView)itemView.findViewById(R.id.time);
            name = (TextView)itemView.findViewById(R.id.name);
            upvotes= (TextView)itemView.findViewById(R.id.upvotes);
            downvotes = (TextView)itemView.findViewById(R.id.downvotes);
            description = (TextView)itemView.findViewById(R.id.description);
            dp = (ImageView) itemView.findViewById(R.id.profilepic);
            try {
                gridView = (MyGridView) itemView.findViewById(R.id.grid);
                comments = (LinearLayout) itemView.findViewById(R.id.comments);
                addcomment= (EditText) itemView.findViewById(R.id.addComment);
                postcomment = itemView.findViewById(R.id.postComment);
                upvote = itemView.findViewById(R.id.upimg);
                downvote = itemView.findViewById(R.id.downimg);
                resolve = itemView.findViewById(R.id.resolve);
                tags = (LinearLayout) itemView.findViewById(R.id.tags);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    List<ComplaintParse> elements;
    Context mContext;
    int openComp=0;
    ArrayList<Bitmap> bitmaps;
    RVAdapterComplaints(List<ComplaintParse> elements){
        this.elements = elements;
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    @Override
    public int getItemViewType(int i) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        if(openComp-1==i) return 1;
        else return 0;
    }


    @Override
    public ElementHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.complaintview, viewGroup, false);
        if(i==1) v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.complaintdetails, viewGroup, false);
        ElementHolder eh = new ElementHolder(v);
        mContext = viewGroup.getContext();
        return eh;
    }
    @Override
    public void onViewDetachedFromWindow(ElementHolder elementHolder)
    {
    }
    @Override
    public void onBindViewHolder(final ElementHolder elementHolder,final int i) {
        elementHolder.title.setText(elements.get(i).getTitle());
        elementHolder.description.setText(elements.get(i).getDescription());
        elementHolder.upvotes.setText(elements.get(i).getUpvotes()+"");
        elementHolder.downvotes.setText(elements.get(i).getDownvotes()+"");
        elementHolder.time.setText(elements.get(i).getFormattedTime());
        elementHolder.name.setText("- by "+elements.get(i).getUsername());

        String url = APIdetails.getHost()+"/uploads/dp/"+elements.get(i).getDpUrl();
        ImageRequest req = new ImageRequest(url,
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        elementHolder.dp.setImageBitmap(bitmap);
                    }
                }, 0, 0, null,
                new Response.ErrorListener() {
                    public void onErrorResponse(VolleyError error) {
                        elementHolder.dp.setImageResource(R.drawable.avatar_42f2a3cb5734_128);
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                SharedPreferences sharedPreferences = mContext.getSharedPreferences("userData",Context.MODE_PRIVATE);
                params.put("authorization", sharedPreferences.getString("token", ""));
                return params;
            }
        };
        VolleySingleton.getInstance(mContext).addToRequestQueue(req);

        if(i==openComp-1)
        {
            SharedPreferences sharedPreferences;
            sharedPreferences = mContext.getSharedPreferences("userData", Context.MODE_PRIVATE);
            String userType = sharedPreferences.getString("userType", "");
            Log.e("mew", elements.get(i).level + " "+userType+" "+(elements.get(i).level.equals("0")&&!userType.equals("3")));
            if((elements.get(i).level.equals("0")&&!userType.equals("3"))||userType.equals("4")||userType.equals("5")) elementHolder.resolve.setVisibility(View.VISIBLE);
            elementHolder.resolve.setOnClickListener(new View.OnClickListener() {
                SharedPreferences sharedPreferences = mContext.getSharedPreferences("userData", Context.MODE_PRIVATE);

                @Override
                public void onClick(View v) {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, APIdetails.resolve(),
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        Log.d("json", response);
                                        JSONObject jsonResponse = new JSONObject(response);
                                        if (jsonResponse.getBoolean("success")) {
                                            elementHolder.resolve.setBackground(mContext.getResources().getDrawable(R.drawable.roundedbg));
                                        } else
                                            Snackbar.make(elementHolder.resolve, jsonResponse.getString("message"), Snackbar.LENGTH_LONG).show();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            // Error handling
                            volleyError.printStackTrace();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
                            params.put("id", elements.get(i).getComplaint_id());
                            return params;

                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("authorization", sharedPreferences.getString("token", ""));
                            return params;
                        }
                    };

                    // Add the request to the queue
                    VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);


                }
            });

            final String[] imurls = elements.get(i).getImages();
            bitmaps = new ArrayList<>();
            elementHolder.gridView.setAdapter(new ImageAdapter(mContext, bitmaps));
            for (int j=0;j<imurls.length;j++) {
                ImageRequest request = new ImageRequest(APIdetails.getHost()+"/uploads/"+imurls[j],
                        new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap bitmap) {
                                bitmaps.add(bitmap);
                                if(bitmaps.size()==imurls.length){
                                    elementHolder.gridView.setAdapter(new ImageAdapter(mContext, bitmaps));
                                }

                            }
                        }, 0, 0, null,
                        new Response.ErrorListener() {
                            public void onErrorResponse(VolleyError error) {
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        SharedPreferences sharedPreferences = mContext.getSharedPreferences("userData", Context.MODE_PRIVATE);
                        params.put("authorization", sharedPreferences.getString("token", ""));
                        return params;
                    }
                };
                VolleySingleton.getInstance(mContext).addToRequestQueue(request);
            }

            setupTags(elements.get(i).tags, elementHolder.tags);
            elementHolder.upvote.setOnClickListener(new View.OnClickListener() {
                SharedPreferences sharedPreferences = mContext.getSharedPreferences("userData", Context.MODE_PRIVATE);

                @Override
                public void onClick(View v) {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, APIdetails.vote(),
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        Log.d("json", response);
                                        JSONObject jsonResponse = new JSONObject(response);
                                        if (jsonResponse.getBoolean("success")) {
                                            elementHolder.upvotes.setText(jsonResponse.getInt("upvotes") + "");
                                            elementHolder.downvotes.setText(jsonResponse.getInt("downvotes") + "");
                                            elements.get(i).upvotes = jsonResponse.getInt("upvotes");
                                            elements.get(i).downvotes = jsonResponse.getInt("downvotes");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            // Error handling
                            volleyError.printStackTrace();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
                            params.put("vote", "1");
                            params.put("id", elements.get(i).getComplaint_id());
                            return params;

                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("authorization", sharedPreferences.getString("token", ""));
                            return params;
                        }
                    };

                    // Add the request to the queue
                    VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);


                }
            });
            elementHolder.downvote.setOnClickListener(new View.OnClickListener() {
                SharedPreferences sharedPreferences = mContext.getSharedPreferences("userData", Context.MODE_PRIVATE);

                @Override
                public void onClick(View v) {
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, APIdetails.vote(),
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    try {
                                        Log.d("json", response);
                                        JSONObject jsonResponse = new JSONObject(response);
                                        if (jsonResponse.getBoolean("success")) {
                                            elementHolder.upvotes.setText(jsonResponse.getInt("upvotes") + "");
                                            elementHolder.downvotes.setText(jsonResponse.getInt("downvotes") + "");
                                            elements.get(i).upvotes = jsonResponse.getInt("upvotes");
                                            elements.get(i).downvotes = jsonResponse.getInt("downvotes");
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            // Error handling
                            volleyError.printStackTrace();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();
                            params.put("vote", "0");
                            params.put("id", elements.get(i).getComplaint_id());
                            return params;

                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            params.put("authorization", sharedPreferences.getString("token", ""));
                            return params;
                        }
                    };

                    // Add the request to the queue
                    VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);


                }
            });
            elementHolder.postcomment.setOnClickListener(new View.OnClickListener() {
                SharedPreferences sharedPreferences = mContext.getSharedPreferences("userData", Context.MODE_PRIVATE);

                @Override
                public void onClick(View v) {
                    {
                        String body = elementHolder.addcomment.getText().toString();
                        if (body.equals("")) return;
                        // Request a string response
                        StringRequest stringRequest = new StringRequest(Request.Method.POST, APIdetails.comment(),
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            Log.d("json", response);
                                            JSONObject jsonResponse = new JSONObject(response);
                                            if (jsonResponse.getBoolean("success")) {
                                                String body = elementHolder.addcomment.getText().toString();
                                                elementHolder.addcomment.setText("");
                                                elements.get(i).comments.add(0, new CommentParse(sharedPreferences.getString("firstName", "") + " " + sharedPreferences.getString("lastName", ""), body, sharedPreferences.getString("dpUrl", "")));
                                                notifyItemChanged(i);
                                                hideKeyboardFrom(mContext,elementHolder.addcomment);
                                                //intent.putExtra("json_user", jsonResponse.get);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError volleyError) {
                                // Error handling
                                volleyError.printStackTrace();
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<>();
                                String body = elementHolder.addcomment.getText().toString();
                                params.put("body", body);
                                params.put("id", elements.get(i).getComplaint_id());
                                return params;

                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String, String> params = new HashMap<>();
                                params.put("authorization", sharedPreferences.getString("token", ""));
                                return params;
                            }
                        };

                        // Add the request to the queue
                        VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);

                    }
                }
            });
            elementHolder.comments.removeAllViews();
            for (int position=0;position<elements.get(i).comments.size();position++)
            {
                ArrayList<CommentParse> values = elements.get(i).comments;
                LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View rowView = inflater.inflate(R.layout.comment, elementHolder.comments, false);
                TextView name = (TextView) rowView.findViewById(R.id.name);
                TextView time = (TextView) rowView.findViewById(R.id.time);
                TextView description = (TextView) rowView.findViewById(R.id.description);
                final ImageView imageView = (ImageView) rowView.findViewById(R.id.profilepic);
                name.setText(values.get(position).name);
                description.setText(values.get(position).description);
                time.setText(TimeHelper.timeFromNow(values.get(position).timestamp,1));
                ImageRequest request = new ImageRequest(APIdetails.dp()+"/"+values.get(position).dpUrl,
                        new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap bitmap) {
                                imageView.setImageBitmap(bitmap);
                            }
                        }, 0, 0, null,
                        new Response.ErrorListener() {
                            public void onErrorResponse(VolleyError error) {
                                imageView.setImageResource(R.drawable.avatar_42f2a3cb5734_128);
                            }
                        }){
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        SharedPreferences sharedPreferences = mContext.getSharedPreferences("userData",Context.MODE_PRIVATE);
                        params.put("authorization", sharedPreferences.getString("token", ""));
                        return params;
                    }
                };
                VolleySingleton.getInstance(mContext).addToRequestQueue(request);

                elementHolder.comments.addView(rowView);
            }
        }
    }
    @Override
    public int getItemCount() {
        return elements.size();
    }

    public void openComplaint(int position)
    {
        int oldComp = openComp;
        if(openComp==position+1) openComp = 0;
        else openComp=position+1;
        notifyItemChanged(position);
        if(oldComp>0)notifyItemChanged(oldComp-1);
    }
    void setupTags(String[] tags,LinearLayout tags_layout )
    {
        tags_layout.removeAllViews();
        int screenSize = DisplayHelper.getWidth(mContext);
        LinearLayout tags_row = new LinearLayout(mContext);
        tags_row.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        tags_row.setOrientation(LinearLayout.HORIZONTAL);
        TextView tag = null;
        LinearLayout.LayoutParams tagParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        int margin = DisplayHelper.dpToPx(10,mContext);
        int padding = DisplayHelper.dpToPx(10, mContext);
        int textSize = DisplayHelper.dpToPx(6, mContext);
        tagParams.bottomMargin = margin;
        tagParams.leftMargin = margin;
        for(int i=0,j=0;i<tags.length+1;i++)
        {
            if(i==tags.length&&j<screenSize)
            {if(tag!=null&&tag.getParent()==null)tags_row.addView(tag);}
            else if(j<screenSize){
                if(tag!=null&&tag.getParent()==null) tags_row.addView(tag);
                tag = new TextView (mContext);
                tag.setLayoutParams(tagParams);
                tag.setPadding(padding, margin / 2, padding, margin / 2);
                tag.setText(tags[i]);
                tag.setTextSize(textSize);
                tag.setClickable(true);
                tag.setTextColor(mContext.getResources().getColor(R.color.accent));
                tag.setBackground(mContext.getResources().getDrawable(R.drawable.roundedbg));
                tag.measure(0, 0);       //must call measure!
                j+=tag.getMeasuredWidth()+margin;
            }
            else {
                j=0;
                i--;
                tags_layout.addView(tags_row);
                tags_row = new LinearLayout(mContext);
                tags_row.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tags_row.setOrientation(LinearLayout.HORIZONTAL);
                tags_row.addView(tag);
                tag.measure(0, 0);       //must call measure!
                j+=tag.getMeasuredWidth()+margin;
            }
        }
        tags_layout.addView(tags_row);

    }
    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}

