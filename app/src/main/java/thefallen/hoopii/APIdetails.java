package thefallen.hoopii;

/**
 * Details for all API endpoints
 **/
public class APIdetails {
    static String host = "http://10.0.0.3:8080/api";
    public static String login()
    {
        return host+"/login";
    }
    public static String addUser()
    {
        return host+"/addUser";
    }
    public static String history()
    {
        return host+"/history";
    }
    public static String resolve()
    {
        return host+"/resolve";
    }
    public static String addComplaint()
    {
        return host+"/postComplain";
    }
    public static String updatedp()
    {
        return host+"/updateDP";
    }
    public static String vote() {return host+"/vote";}
    public static String comment() {
        return host+"/postComment";
    }
    public static String getHost() {
        return host;
    }
    public static String dp() {
        return host+"/uploads/dp";
    }
}
