package thefallen.hoopii;

import android.app.SearchManager;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to aw
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private Context mContext;
    public state State;
    private SharedPreferences sharedPreferences;
    private String userType;
    SearchView searchView;
    SectionsPagerAdapter mSectionsPagerAdapter;

    public enum state
    {
        HOSTEL,INSTI
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        userType = sharedPreferences.getString("userType", "");
        if(userType.equals("3")) {
            Intent intent = new Intent(this,History.class);
            startActivity(intent);
            finish();
        }
        else if(userType.equals("4"))
            State = state.HOSTEL;
        else
        State = state.INSTI;

    }

    public void changeView(){
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        mViewPager.setCurrentItem(0);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (userType.equals("4") || userType.equals("5"))
            getMenuInflater().inflate(R.menu.menu_main2, menu);
        else
            getMenuInflater().inflate(R.menu.menu_main, menu);
        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        changeView();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if (State==state.INSTI)
                State=state.HOSTEL;
            else State=state.INSTI;
            changeView();
        }
        else if(id == R.id.action_logOut)
        {
            sharedPreferences = mContext.getSharedPreferences("userData", MODE_PRIVATE);
            SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
            sharedPreferencesEditor.putString("token","");
            sharedPreferencesEditor.putString("password", "");
            sharedPreferencesEditor.apply();
            finish();
            startActivity(new Intent(mContext,LoginActivity.class));
        }
        else if(id == R.id.action_profile)
        {
            Intent intent = new Intent(mContext,ProfileActivity.class);
            startActivity(intent);
        }
        else if(id == R.id.action_history)
        {
            Intent intent = new Intent(mContext,History.class);
            startActivity(intent);
        }

        else if(id == R.id.action_postComplaint)
        {
            Intent intent = new Intent(mContext,PostComplaint.class);
            startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
//        private static final String ARG_STATE= "state";
        private SharedPreferences sharedPreferences;
        private ArrayList<thefallen.hoopii.ComplaintParse> complaints;
        private ArrayList<thefallen.hoopii.ComplaintParse> complaints_original;
        ArrayList<ComplaintParse> searchResult;
        private RecyclerView rv;
        SwipeRefreshLayout swipeLayout;
        RVAdapterComplaints adapter;
        String category = "";
        MainActivity activity;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            sharedPreferences = this.getActivity().getSharedPreferences("userData", MODE_PRIVATE);

            activity = (MainActivity) getActivity();

            int cat = getArguments().getInt(ARG_SECTION_NUMBER);
            if(cat==1) category="hot";
            if(cat==2) category="trending";
            if(cat==3) category="fresh";
            getComplaints(category);

            swipeLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefresh);
            swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getComplaints(category);
                }
            });

            swipeLayout.setColorSchemeColors(R.color.primary, R.color.primary_dark, R.color.primary_darker, R.color.black);
            rv = (RecyclerView) rootView.findViewById(R.id.rv);
            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            rv.setLayoutManager(llm);
            rv.setHasFixedSize(true);
            ItemClickSupport.addTo(rv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                    RVAdapterComplaints adapter = (RVAdapterComplaints) recyclerView.getAdapter();
                    adapter.openComplaint(position);
                    Log.d("rv", recyclerView.getChildCount() + "");
                }
            });
            if(getUserVisibleHint()) {
                activity.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                                @Override
                                public boolean onQueryTextSubmit(String query) {
                                    adapter.elements = Search.search(complaints_original, query);
                                    adapter.notifyDataSetChanged();
                                    return false;
                                }

                                @Override
                                public boolean onQueryTextChange(String newText) {
                                    adapter.elements = Search.search(complaints_original, newText);
                                    adapter.notifyDataSetChanged();
                                    return false;
                                }
                            });
            }

            return rootView;

        }
        @Override
        public void setUserVisibleHint(boolean isVisibleToUser) {
            super.setUserVisibleHint(isVisibleToUser);
            if(isVisibleToUser&&activity!=null&&activity.searchView!=null)
                activity.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String query) {
                    adapter.elements = Search.search(complaints_original, query);
                    adapter.notifyDataSetChanged();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    adapter.elements = Search.search(complaints_original, newText);
                    adapter.notifyDataSetChanged();
                    return false;
                }
            });
        }        // Call all APIs for all courses threads, then sort them and show in recyclerView
        public void getComplaints(final String category)
        {
            String url;
            if(((MainActivity)this.getActivity()).State== MainActivity.state.HOSTEL)
                url = APIdetails.getHost()+"/hostel/"+category;
            else
                url = APIdetails.getHost()+"/insti/"+category;
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("response",response);
                            try {
                                JSONArray comps = new JSONArray(response);
                                complaints = new ArrayList<>();
                                    for (int i = 0; i < comps.length(); i++)
                                        complaints.add(new thefallen.hoopii.ComplaintParse(comps.getJSONObject(i)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Collections.reverse(complaints);
                            complaints_original = new ArrayList<>(complaints);
                            adapter = new RVAdapterComplaints(complaints);
                            rv.setAdapter(adapter);
                            swipeLayout.setRefreshing(false);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                }
            })
            {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("authorization", sharedPreferences.getString("token", ""));
                    return params;
                }
            };
            // Add the request to the RequestQueue.
            VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);

        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Hot";
                case 1:
                    return "Trending";
                case 2:
                    return "Fresh";
            }
            return null;
        }
    }
}
