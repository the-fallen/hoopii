package thefallen.hoopii;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Helps create a grid of images when we need to display multiple images
 */

public class GridImageAdapter extends RecyclerView.Adapter<GridImageAdapter.ElementHolder> {

public static class ElementHolder extends RecyclerView.ViewHolder {

    ImageView imageView;
    TextView metadata;

    ElementHolder(View itemView) {
        super(itemView);
        imageView = (ImageView)itemView.findViewById(R.id.image);
        metadata = (TextView)itemView.findViewById(R.id.metadata);
    }
}

List<imageItem> elements;
Context context;
    GridImageAdapter(List<imageItem> elements){
        this.elements = elements;
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }



    @Override
    public ElementHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.image_element, viewGroup, false);
        ElementHolder eh = new ElementHolder(v);
        context = viewGroup.getContext();
        return eh;
    }
    @Override
    public void onViewDetachedFromWindow(ElementHolder elementHolder)
    {
    }
    @Override
    public void onBindViewHolder(ElementHolder elementHolder, int i) {
        elementHolder.imageView.setImageBitmap(elements.get(i).bitmap);
        elementHolder.metadata.setText(elements.get(i).metadata);
    }

    @Override
    public int getItemCount() {
        if (elements!=null) return elements.size();
        return 0;
    }

    public void removeAt(int position) {
        elements.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, elements.size());
    }
}
