package thefallen.hoopii;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
/*
    The activity to display the complaints a user has previously posted
*/

public class History extends AppCompatActivity {

    private SharedPreferences sharedPreferences;
    private ArrayList<ComplaintParse> complaints;
    RecyclerView rv;
    SwipeRefreshLayout swipeLayout;
    RVAdapterComplaints adapter;
    SearchView searchView;
    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        String userType=sharedPreferences.getString("userType", "");
        if(userType.equals("3")){
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setTitle("Hoopii");
        }
        mContext = this;

        //sets up the swipe layout
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getComplaints();
            }
        });
        swipeLayout.setColorSchemeColors(R.color.primary, R.color.primary_dark, R.color.primary_darker, R.color.black);

        //Sets up the recycler view
        rv = (RecyclerView) findViewById(R.id.rv);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        ItemClickSupport.addTo(rv).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                RVAdapterComplaints adapter = (RVAdapterComplaints)recyclerView.getAdapter();
                adapter.openComplaint(position);
            }
        });

        getComplaints();
    }

    /*
        input : void
        output : void
        function : calls the api to get all the complaints
     */
    void getComplaints(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, APIdetails.history(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("history_response", response);
                        try {
                            JSONArray comps = new JSONArray(response);
                            complaints = new ArrayList<>();
                            for (int i = 0; i < comps.length(); i++)
                                complaints.add(new ComplaintParse(comps.getJSONObject(i)));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapter = new RVAdapterComplaints(complaints);
                        rv.setAdapter(adapter);
                        swipeLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        })
        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authorization", sharedPreferences.getString("token", ""));
                return params;
            }
        };
        // Add the request to the RequestQueue.
        VolleySingleton.getInstance(mContext).addToRequestQueue(stringRequest);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history, menu);

        searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                rv.setAdapter(new RVAdapterComplaints(Search.search(complaints,query)));
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                rv.setAdapter(new RVAdapterComplaints(Search.search(complaints,newText)));
                return false;
            }
        });
        return true;
    }

    /*
        input : item - MenuItem
        output : boolean
        function : takes action depending on the menu option selected
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == android.R.id.home)
            finish();
        else if(id == R.id.action_logOut)
        {
            sharedPreferences = mContext.getSharedPreferences("userData", MODE_PRIVATE);
            SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
            sharedPreferencesEditor.putString("token","");
            sharedPreferencesEditor.putString("password", "");
            sharedPreferencesEditor.apply();
            finish();
            startActivity(new Intent(mContext,LoginActivity.class));
        }
        else if(id == R.id.action_profile)
        {
            Intent intent = new Intent(mContext,ProfileActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }


}
