package thefallen.hoopii;

import android.util.Log;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by root on 3/25/16.
 */
public class Search {
    public static ArrayList<ComplaintParse> search(ArrayList<ComplaintParse> original,String searchText) {
        ArrayList<ComplaintParse> clone = new ArrayList<>(original);
        ArrayList<ComplaintParse> result = new ArrayList<>();
        for(int i=0;i<clone.size();i++) if(clone.get(i).title.replaceAll("\\s","").toLowerCase().contains(searchText.replaceAll("\\s","").toLowerCase())) result.add(clone.remove(i--));
        for(int i=0;i<clone.size();i++) if(Arrays.toString(clone.get(i).tags).replaceAll("\\s","").toLowerCase().contains(searchText.replaceAll("\\s","").toLowerCase())) result.add(clone.remove(i--));
        for(int i=0;i<clone.size();i++) if(clone.get(i).getDescription().replaceAll("\\s", "").toLowerCase().contains(searchText.replaceAll("\\s","").toLowerCase())) result.add(clone.remove(i--));

        return result;

    }
}
