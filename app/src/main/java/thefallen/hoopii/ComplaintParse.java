package thefallen.hoopii;

/**
 *Interprets the data we get for the complaints from the api and parse it into meaningful usable variables
 */

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class ComplaintParse {
    String level,complaint_id;
    int upvotes,downvotes;
    String entryno,name,title,location,status,description,dpUrl;
    Timestamp timestamp = null;
    ArrayList<CommentParse> comments;
    String[] images,tags;
    public ComplaintParse(JSONObject jsonObject)
    {
        try {
            complaint_id = jsonObject.getString("_id");
            level = jsonObject.getString("level");
            upvotes = jsonObject.getInt("upvotes");
            downvotes = jsonObject.getInt("downvotes");
            description = jsonObject.getString("description");
            name = jsonObject.getString("firstName")+" "+ jsonObject.getString("lastName");
            title = jsonObject.getString("title");
            dpUrl = jsonObject.getString("dpUrl");
            entryno = jsonObject.getString("entryno");
            location= jsonObject.getString("location");
            status = jsonObject.getString("status");
            timestamp = new Timestamp(Long.valueOf(jsonObject.getString("timestamp"))*1000);
            comments = CommentParse.CommentArray(jsonObject.getJSONArray("comments"));
            JSONArray jsonArray = jsonObject.getJSONArray("tags");
            tags = new String[jsonArray.length()];
            for (int i=0;i<jsonArray.length();i++)
                tags[i] = (String)jsonArray.get(i);
            jsonArray = jsonObject.getJSONArray("images");
            images = new String[jsonArray.length()];
            for (int i=0;i<jsonArray.length();i++)
                images[i] = (String)jsonArray.get(i);
        }catch (Exception e){
            e.printStackTrace();
            complaint_id=level=description=title=entryno=location=status="";
            upvotes=downvotes=0;
        }
    }

    public String getComplaint_id() {
        return complaint_id;
    }
    public String getLevel() {
        return level;
    }
    public int getUpvotes() {
        return upvotes;
    }
    public int getDownvotes() {
        return downvotes;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getEntryno() {
        return entryno;
    }

    public String getLocation() {
        return location;
    }

    public String getStatus(){
        return status;
    }

    public String getDpUrl() {
        return dpUrl;
    }

    public String[] getImages() {
        return images;
    }

    public String getUsername() {
        return name;
    }

    public String getFormattedTime(){
            return TimeHelper.timeFromNow(timestamp,1);
    }
}

