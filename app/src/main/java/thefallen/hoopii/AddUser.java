package thefallen.hoopii;
/*
 * Activity handling adding users to the database by the superUser
 */
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import android.app.ProgressDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

    public class AddUser extends AppCompatActivity {

    // Basic Declarations
    private static final String TAG = "AddUser";
    private static final int REQUEST_CODE = 1;
    Bitmap bitmap;
    Context mContext;
    SharedPreferences sharedPreferences;

    @InjectView(R.id.input_name) EditText _nameText;
    @InjectView(R.id.input_email) EditText _entryNo;
    @InjectView(R.id.input_password) EditText _passwordText;
    @InjectView(R.id.btn_signup) Button _signupButton;
    @InjectView(R.id.location) Spinner spinnerLoc;
    @InjectView(R.id.userType) Spinner spinnerTyp;
    @InjectView(R.id.dp) ImageView displayPic;
    String location;
    int userType;
    ProgressDialog progressDialog;
    @Override

    public void onCreate(Bundle savedInstanceState) {

        // Setup the display
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);
        ButterKnife.inject(this);
        mContext = this;
        sharedPreferences = getSharedPreferences("userData", MODE_PRIVATE);
        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });
        displayPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImage();
            }
        });
        // default display picture
        bitmap =  BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.avatar_42f2a3cb5734_128);
        addItemsToSpinners();

    }
    /*
    input  : void
    output : void
    function: validates post data and then calls the API
     */
    public void signup() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            onSignupFailed();
            return;
        }

        _signupButton.setEnabled(false);

        progressDialog = new ProgressDialog(AddUser.this,
                R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();

        callApi();
    }

    /*
    input : void
    output : void
    function : Dismisses progressDialog and enables the sign up button
     */

    public void onSignupSuccess() {
        _signupButton.setEnabled(true);
        if(progressDialog.isShowing())
            progressDialog.dismiss();
    }

    /*
    input : void
    output : void
    function : Dismisses progressDialog and enables the sign up button
     */
    public void onSignupFailed() {
        if(progressDialog!=null)if(progressDialog.isShowing()) progressDialog.dismiss();
        _signupButton.setEnabled(true);
    }



    /*
        input : void
        output : void
        function : Checks the format of the inputs that are being given to the form
     */
    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String entryNo = _entryNo.getText().toString();
        String password = _passwordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.setError("at least 3 characters");
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (entryNo.isEmpty()) {
            _entryNo.setError("enter a valid entry no");
            valid = false;
        } else {
            _entryNo.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 ) {
            _passwordText.setError("greater than 4 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    /*
        input : void
        output : void
        function : populates the spinners
     */

    public void addItemsToSpinners()
        {
            ArrayList<String> list = new ArrayList<>();
            list.add("Aravali");
            list.add("Girnar");
            list.add("Himadri");
            list.add("Jwalamukhi");
            list.add("Kailash");
            list.add("Karakoram");
            list.add("Kumaon");
            list.add("Nilgiri");
            list.add("Shivalik");
            list.add("Udaigiri");
            list.add("Vindhyachal");
            list.add("Zanskar");
            list.add("Taxila Apts");
            list.add("Vikramshila");
            addItemsToSpinner(list, spinnerLoc,0);
            list = new ArrayList<>();
            list.add("Superuser");
            list.add("Student");
            list.add("Faculty");
            list.add("Worker");
            list.add("Warden");
            list.add("Dean");
            addItemsToSpinner(list,spinnerTyp,1);


        }

    /*
        input : list - ArrayList<String>, spinner - Spinner, type - int
        output : void
        function : sets the variables to be sent depending on the position of the spinner
     */

    public void addItemsToSpinner(ArrayList<String> list,Spinner spinner,int type) {

            // Custom ArrayAdapter with spinner item layout to set popup background

            CustomSpinnerAdapter spinAdapter = new CustomSpinnerAdapter(
                    getApplicationContext(), list);
            spinner.setAdapter(spinAdapter);
            if(type==0) spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> adapter, View v,
                                           int position, long id) {
                    // On selecting a spinner item
                    location = adapter.getItemAtPosition(position).toString();
                    // Showing selected spinner item
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });
            else spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> adapter, View v,
                                           int position, long id) {
                    userType = position;
                    // Showing selected spinner item
                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                }
            });
        }

    /*
        input : void
        output : void
        function : starts the activity to select an image
     */
    public void pickImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, REQUEST_CODE);
    }

    /*
        input : requestCode - int, resultCode - int, data - Intent
        output : void
        function : extracts the image from the result of the image picking
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    try {
                        bitmap = decodesmall(data.getData(),512);
                        displayPic.setImageBitmap(bitmap);
                    } catch (Exception e) {
                        Log.e("im", e.getLocalizedMessage());
                    }

                    break;

                }
        }
    }

    /*
    input : uri - Uri, factor - int
    output : Bitmap
    function : standardises the image that we get
    */
    public Bitmap decodesmall(Uri uri,int factor) {
        ParcelFileDescriptor parcelFD = null;
        try {
            parcelFD = getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor imageSource = parcelFD.getFileDescriptor();

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFileDescriptor(imageSource, null, o);

            // the new size we want to scale to

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp < factor && height_tmp < factor) {
                    break;
                }
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFileDescriptor(imageSource, null, o2);

        } catch (FileNotFoundException e) {
            // handle errors
        } finally {
            if (parcelFD != null)
                try {
                    parcelFD.close();
                } catch (IOException e) {
                    // ignored
                }
        }
        return null;
    }

    @Override
    public void onBackPressed(){
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putString("token","");
        sharedPreferencesEditor.putString("password", "");
        sharedPreferencesEditor.apply();
        startActivity(new Intent(mContext, LoginActivity.class));
        super.onBackPressed();
    }
    @Override
    public void onDestroy()
    {
    SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
    sharedPreferencesEditor.putString("token","");
    sharedPreferencesEditor.putString("password", "");
    sharedPreferencesEditor.apply();
    super.onDestroy();
    }

    /*
        input : void
        output : void
        function : calls the api to post the data
     */


    public void callApi()
    {
        ArrayList<Bitmap> files = new ArrayList<>();
        files.add(bitmap);
        MultipartRequest multipartRequest = new MultipartRequest(APIdetails.addUser(), null,"avatar",files, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    JSONObject jsonObject = (new JSONObject(new String(response.data)));
                    if (jsonObject.getBoolean("success"))
                        onSignupSuccess();
                    else {
                        Snackbar.make(_signupButton, jsonObject.getString("message"), Snackbar.LENGTH_SHORT).show();
                        onSignupFailed();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onSignupFailed();
            }
        })
        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                String name = _nameText.getText().toString();
                String firstName = name.split(" ",2)[0];
                String lastName =" ";
                if(name.split(" ",2).length>1)lastName = name.split(" ",2)[1];
                params.put("firstName", firstName);
                params.put("lastName", lastName);
                params.put("entryno", _entryNo.getText().toString());
                params.put("location", location);
                params.put("password", _passwordText.getText().toString());
                params.put("userType", userType+"");
                return params;

            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("authorization", sharedPreferences.getString("token",""));
                return params;
            }
        };

        VolleySingleton.getInstance(mContext).addToRequestQueue(multipartRequest);
    }

}
